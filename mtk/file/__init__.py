# -*- coding: utf-8 -*-
# @Date     : 2021/4/8 11:41
# @Author   : xiaochuang.wu
# @File     : __init__.py.py

import os


def make_directory(directory):
    parent, name = os.path.split(directory)
    exist = os.path.exists(directory)
    if not exist:
        if make_directory(parent):
            try:
                os.mkdir(directory)
                exist = True
            except IOError as e:
                exist = False
    return exist