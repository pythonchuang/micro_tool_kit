from abc import ABC
from types import FunctionType
from sanic import Blueprint


class BaseBluePrint(Blueprint, ABC):
    bp_list = list()

    def __init__(
            self,
            name,
            url_prefix=None,
            host=None,
            version=None,
            strict_slashes=None,
    ):
        super().__init__(name, url_prefix=url_prefix, host=host, version=version, strict_slashes=strict_slashes)
        self.bp_list.append(self)

    def rt(
        self,
        uri,
        methods=frozenset({"GET"}),
        host=None,
        strict_slashes=None,
        stream=False,
        version=None,
        name=None,
    ):
        def decorator(handler):
            if isinstance(handler, FunctionType):
                return self.route(uri, methods=methods, host=host, strict_slashes=strict_slashes,
                                  stream=stream, version=version, name=name)(handler)
            else:
                self.add_route(handler.as_view(), uri, methods=methods, host=host, strict_slashes=strict_slashes,
                                      version=version, name=name, stream=stream)
                return handler
        return decorator
