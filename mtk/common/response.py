# -*- coding: utf-8 -*-
# @author: dengxixi
# @date:   2021-03-10
# @file:   

from sanic.response import json


def success():
    return json(dict(code=0, msg="ok"))


def param_lack():
    return json(dict(code=10000, msg='参数缺失'))


def param_error():
    return json(dict(code=10001, msg='参数错误'))


def not_login_resp():
    return json(dict(code=30000, msg="未登录, 显示登录窗口"))