
import os
import sys
from mtk.file import make_directory


def get_logging_config(pid, log_file_path=None, level="INFO"):
    level = level.upper().strip()
    base_path = log_file_path or os.path.join(os.path.dirname(__file__), "../../logs")
    make_directory(base_path)
    _path = os.path.join(base_path, "server_{}.log".format(pid))
    _LOGGING_CONFIG = dict(
        version=1,
        disable_existing_loggers=False,

        loggers={
            "root": {
                "level": level,
                "handlers": ["console", "debug_console"] if level == "DEBUG" else ["console"]
            },
            "sanic.error": {
                "level": "INFO",
                "handlers": ["error_console"],
                "propagate": True,
                "qualname": "sanic.error"
            },

            "sanic.access": {
                "level": "INFO",
                "handlers": ["access_console"],
                "propagate": True,
                "qualname": "sanic.access"
            }
        },
        handlers={
            "console": {
                "class": "logging.handlers.TimedRotatingFileHandler",
                "formatter": "generic",
                "when": "midnight",
                "interval": 1,
                "backupCount": 180,
                "filename": _path,
            },
            "error_console": {
                "class": "logging.StreamHandler",
                "formatter": "generic",
                "stream": sys.stderr
            },
            "access_console": {
                "class": "logging.StreamHandler",
                "formatter": "access",
                "stream": sys.stdout
            },
            "debug_console": {
                "class": "logging.StreamHandler",
                "formatter": "generic",
                "stream": sys.stdout
            }
        },
        formatters={
            "generic": {
                "format": "[%(asctime)s] %(levelname)s (%(filename)s:%(lineno)d) %(message)s",
                # "datefmt": "[%Y-%m-%d %H:%M:%S %z]",
                "class": "logging.Formatter"
            },
            "access": {
                "format": "%(asctime)s - (%(name)s)[%(levelname)s][%(host)s]: " +
                          "%(request)s %(message)s %(status)d %(byte)d",
                "datefmt": "[%Y-%m-%d %H:%M:%S %z]",
                "class": "logging.Formatter"
            },
        }
    )

    return _LOGGING_CONFIG

