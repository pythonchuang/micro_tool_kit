# -*- coding: utf-8 -*-
# @author: dengxixi
# @date:   2021-03-10
# @file:   

from .listener import register_listener
from .excetion import error_handler


def init_app(app):
    register_listener(app)
    error_handler(app)
