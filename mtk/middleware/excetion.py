# -*- coding: utf-8 -*-
# @author: dengxixi
# @date:   2021-03-10
# @file:   捕获异常
from sanic.exceptions import NotFound
from sanic.response import json
import logging

logger = logging.getLogger("root")


def ignore_404_page(request, exception):
    return json(dict(url=request.url, msg='Not Found, check route exists'), status=404)


def server_error_handler(request, exception):
    logger.exception(exception)
    return json(dict(msg=str(exception)), status=500)


def error_handler(app):
    app.error_handler.add(NotFound, ignore_404_page)
    # Exception 异常要放在最后添加到app, 不然会覆盖掉其它细分的异常
    app.error_handler.add(Exception, server_error_handler)
